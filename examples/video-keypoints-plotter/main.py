import sys
import face_alignment
import cv2
import numpy as np
from tqdm import trange

from media_processing_lib.video import tryReadVideo, tryWriteVideo
from vke.face import vidGetFaceKeypoints, vidGetFaceBbox
from vke.body import vidGetBodyKeypoints, videoDisplayBodyParts, vidGetBodyBbox
from nwdata.utils import fullPath, smoothBbox
from nwmodule.utilities import device

def videoKeypointer(video, videoKeypoints):
	# video = video.copy()
	radius = int(min(video.shape[1], video.shape[2]) * 1 / 200)
	thickness = -1
	res = []
	for i in trange(len(video), desc="Keypointing video"):
		imgKeypoints = videoKeypoints[i]
		frame = video[i]
		for kp_i, kp_j in zip(imgKeypoints[:, 0], imgKeypoints[:, 1]):
			frame = cv2.circle(frame, (kp_i, kp_j), radius, (0, 0, 255), thickness)
		res.append(frame)
	return np.array(res)

def videoRectangle(video, faceBbox):
	res = np.zeros(video.shape, dtype=np.uint8)
	for i in trange(len(video), desc="Rectangling video"):
		x1, x2, y1, y2 = faceBbox[i]
		res[i] = cv2.rectangle(video[i], (x1, y1), (x2, y2), (0, 0, 0), 2)
	return res

def bodyRectangle(video, bodyBbox):
	res = np.zeros(video.shape, dtype=np.uint8)
	for i in trange(len(video), desc="Body Rectangling video"):
		bboxes = bodyBbox[i]
		nBoxes = len(bboxes)
		res[i] = video[i]
		for j in range(nBoxes):
			try:
				x1, x2, y1, y2 = bboxes[j]
			except Exception:
				breakpoint()
			res[i] = cv2.rectangle(res[i], (x1, y1), (x2, y2), (0, 0, 0), 2)
	return res

def videoPlot(video, faceBbox, faceKeypoints, bodyBbox, bodyKeypoints):
	video = videoRectangle(video, faceBbox)
	video = bodyRectangle(video, bodyBbox)
	video = videoKeypointer(video, faceKeypoints)
	video = videoKeypointer(video, bodyKeypoints["coordinates"])
	video = videoDisplayBodyParts(video, bodyKeypoints["coordinates"])
	return video

def vidSmoothExtremes(video, bbox):
	global smoothBbox
	smoothedBbox = smoothBbox(bbox)
	H, W = video.shape[1], video.shape[2]
	smoothedBbox = np.clip(smoothedBbox, [0, 0, 0, 0], [W - 1, W - 1, H - 1, H - 1])
	return smoothedBbox

def main():
	video = tryReadVideo(sys.argv[1], vidLib="pims")
	print("Read video %s. Shape: %s. FPS: %2.2f" % (sys.argv[1], video.shape, video.fps))
	fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, flip_input=False, device=str(device))
	faceBbox = vidGetFaceBbox(video, faceLib="face_alignment")
	faceKeypoints = vidGetFaceKeypoints(video, faceLib="face_alignment")
	bodyKeypoints = vidGetBodyKeypoints(video, poseLib="efficient_pose_i")
	bodyBbox = vidGetBodyBbox(video, poseLib="yolov5s")
	assert (not faceBbox is None) and (not faceKeypoints is None) and (not bodyKeypoints is None)

	plottedVideo = videoPlot(video, faceBbox, faceKeypoints, bodyBbox, bodyKeypoints)
	smoothedFaceBbox = vidSmoothExtremes(video, faceBbox)
	smoothPlottedVideo = videoPlot(video, smoothedFaceBbox, faceKeypoints, bodyBbox, bodyKeypoints)

	tryWriteVideo(plottedVideo, "keypoint.mp4", fps=video.fps)
	tryWriteVideo(smoothPlottedVideo, "smooth_keypoint.mp4", fps=video.fps)

if __name__ == "__main__":
	main()