from .utils import imageDisplayBodyParts, videoDisplayBodyParts
from .efficient_pose import VKE_EfficientPose
from .yolov5 import VKE_YOLOV5
from ..utils import dprint

Objs = {}

def getLib(poseLib):
    global Objs
    if poseLib in Objs:
        return Objs[poseLib]

    if poseLib == "efficient_pose_rt":
        dprint("[body::getLib] Instantiating EfficientPose RT object.")
        Objs[poseLib] = VKE_EfficientPose("RT")
    elif poseLib == "efficient_pose_i":
        dprint("[body::getLib] Instantiating EfficientPose I object.")
        Objs[poseLib] = VKE_EfficientPose("I")
    elif poseLib == "efficient_pose_ii":
        dprint("[body::getLib] Instantiating EfficientPose II object.")
        Objs[poseLib] = VKE_EfficientPose("II")
    elif poseLib == "efficient_pose_iii":
        dprint("[body::getLib] Instantiating EfficientPose III object.")
        Objs[poseLib] = VKE_EfficientPose("III")
    elif poseLib == "efficient_pose_iv":
        dprint("[body::getLib] Instantiating EfficientPose IV object.")
        Objs[poseLib] = VKE_EfficientPose("IV")
    elif poseLib == "yolov5s":
        dprint("[body::getLib] Instantiating YoloV5S object.")
        Objs[poseLib] = VKE_YOLOV5("S")

    return Objs[poseLib]

def imgGetBodyKeypoints(image, poseLib):
    return getLib(poseLib).imgGetBodyKeypoints(image)

def vidGetBodyKeypoints(video, poseLib):
    return getLib(poseLib).vidGetBodyKeypoints(video)

def vidGetBodyBbox(video, poseLib):
    return getLib(poseLib).vidGetBodyBbox(video)

def imgGetBodyBbox(video, poseLib):
    return getLib(poseLib).imgGetBodyBbox(video)