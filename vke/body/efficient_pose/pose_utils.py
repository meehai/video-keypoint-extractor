import numpy as np
import os
import math
from skimage.transform import rescale
from numpy import pad as padding
from scipy.ndimage.filters import gaussian_filter
from tqdm import trange
from pathlib import Path

from ...utils import videoGetDescriptor, vkeCache, dprint, drange

def extract_coordinates(frame_output, frame_height, frame_width):    	
	# Define confidence level
	confidence = 0.3
	
	# Fetch output resolution 
	output_height, output_width = frame_output.shape[0:2]
	
	# Initialize coordinates
	frame_coords = []
	
	# Iterate over body parts
	for i in range(frame_output.shape[-1]):

		# Find peak point
		conf = frame_output[...,i]
		conf = gaussian_filter(conf, sigma=1.) 
		max_index = np.argmax(conf)
		peak_y = float(math.floor(max_index / output_width))
		peak_x = max_index % output_width
		
		# Verify confidence
		# if real_time and conf[int(peak_y),int(peak_x)] < confidence:
		#     peak_x = -0.5
		#     peak_y = -0.5
		# else:
		peak_x += 0.5
		peak_y += 0.5

		# Normalize coordinates
		peak_x /= output_width
		peak_y /= output_height

		# Convert to original aspect ratio 
		if frame_width > frame_height:
			norm_padding = (frame_width - frame_height) / (2 * frame_width)  
			peak_y = (peak_y - norm_padding) / (1.0 - (2 * norm_padding))
			peak_y = -0.5 / output_height if peak_y < 0.0 else peak_y
			peak_y = 1.0 if peak_y > 1.0 else peak_y
		elif frame_width < frame_height:
			norm_padding = (frame_height - frame_width) / (2 * frame_height)  
			peak_x = (peak_x - norm_padding) / (1.0 - (2 * norm_padding))
			peak_x = -0.5 / output_width if peak_x < 0.0 else peak_x
			peak_x = 1.0 if peak_x > 1.0 else peak_x

		frame_coords.append((peak_x, peak_y))
		
	return frame_coords

def resize(source_array, target_height, target_width):
	""" 
	Resizes an image or image-like Numpy array to be no larger than (target_height, target_width) or (target_height, target_width, c).
	
	Args:
		source_array: ndarray
			Numpy array of shape (h, w) or (h, w, 3)
		target_height: int
			Desired maximum height
		target_width: int
			Desired maximum width
		
	Returns:
		Resized Numpy array.
	"""
	
	# Get height and width of source array
	source_height, source_width = source_array.shape[:2]
	 
	# Compute correct scale for resizing operation
	target_ratio = target_height / target_width
	source_ratio = source_height / source_width
	if target_ratio > source_ratio:
		scale = target_width / source_width
	else:
		scale = target_height / source_height
		
	# Perform rescaling
	resized_array = rescale(source_array, scale, multichannel=True)
	
	return resized_array

def pad(source_array, target_height, target_width):
	""" 
	Pads an image or image-like Numpy array with zeros to fit the target-size.
	
	Args:
		source_array: ndarray
			Numpy array of shape (h, w) or (h, w, 3)
		target_height: int
			Height of padded image
		target_width: int
			Width of padded image
	
	Returns:
		Zero-padded Numpy array of shape (target_height, target_width) or (target_height, target_width, c).
	"""
	
	# Get height and width of source array
	source_height, source_width = source_array.shape[:2]
	
	# Ensure array is resized properly
	if (source_height > target_height) or (source_width > target_width):
		source_array = resize(source_array, target_height, target_width)
		source_height, source_width = source_array.shape[:2]
		
	# Compute padding variables
	pad_left = int((target_width - source_width) / 2)
	pad_top = int((target_height - source_height) / 2)
	pad_right = int(target_width - source_width - pad_left)
	pad_bottom = int(target_height - source_height - pad_top)
	paddings = [[pad_top, pad_bottom], [pad_left, pad_right]]
	has_channels_dim = len(source_array.shape) == 3
	if has_channels_dim:  
		paddings.append([0,0])
		
	# Perform padding
	target_array = padding(source_array, paddings, 'constant')
	
	return target_array

def preprocess(batch, resolution):
	"""
	Preprocess Numpy array according to model preferences.
	
	Args:
		batch: ndarray
			Numpy array of shape (n, h, w, 3)
		resolution: int
			Input height and width of model to utilize
	
	Returns:
		Preprocessed Numpy array of shape (n, resolution, resolution, 3).
	"""
	
	# Resize frames according to side
	batch = [resize(frame, resolution, resolution) for frame in batch]

	# Pad frames in batch to form quadratic input
	batch = [pad(frame, resolution, resolution) for frame in batch]

	# Convert from normalized pixels to RGB absolute values
	batch = [np.uint8(255 * frame) for frame in batch]

	# Construct Numpy array from batch
	batch = np.asarray(batch)

	# Preprocess images in batch
	mean = [0.485, 0.456, 0.406]
	std = [0.229, 0.224, 0.225]
	batch = np.float32(batch) / 255
	batch = (batch - mean) / std

	return batch

def imgGetBodyKeypoints(image, model):
	assert image.dtype == np.uint8 and len(image.shape) == 3 and image.shape[-1] == 3
	frame_height, frame_width = image.shape[0 : 2]
	_img = preprocess(image[None], model.resolution)
	outputs = model.doInference(_img)[0]
	coordinates = np.array(extract_coordinates(outputs, frame_height, frame_width))

	# Define body parts
	body_parts = ['head_top', 'upper_neck', 'right_shoulder', 'right_elbow', 'right_wrist', 'thorax', 'left_shoulder', 'left_elbow', 'left_wrist', 'pelvis', 'right_hip', 'right_knee', 'right_ankle', 'left_hip', 'left_knee', 'left_ankle']

	assert (coordinates < 0).sum() == 0
	assert (coordinates > 1).sum() == 0
	coordinates = np.int32(coordinates * [frame_height, frame_width])

	result = {
		"coordinates" : coordinates,
		"body_parts" : body_parts
	}
	return result


def vidGetBodyKeypoints(video, model, batch_size=1):
	key = "%s/efficient_pose_%s_body_kp.npy" % (videoGetDescriptor(video), model.modelType)
	if vkeCache.check(key):
		vidKeypoints = vkeCache.get(key)
		assert len(vidKeypoints["coordinates"]) == len(video)
		dprint("[EfficientPose%s::vidGetBodyKeypoints] Loaded keypoints from %s" % (model.modelType, key))
		return vidKeypoints
	
	# Define batch size and number of batches in each part
	num_video_frames = len(video)
	num_batches = int(np.ceil(num_video_frames / batch_size))
	frame_height, frame_width = video.shape[1 : 3]

	# Operate on batches
	coordinates = np.zeros((num_video_frames, 16, 2), dtype=np.float32)

	for i in drange(num_batches, desc="[EfficientPose%s]" % (model.modelType)):
		startIndex, endIndex = i * batch_size, min((i + 1) * batch_size, num_video_frames)
		batch = np.array([video[ix] for ix in range(startIndex, endIndex)])

		batch = preprocess(batch, model.resolution)
		batch_outputs = model.doInference(batch)

		# Extract coordinates for batch
		batch_coordinates = [extract_coordinates(kp, frame_height, frame_width) for kp in batch_outputs]
		coordinates[startIndex : endIndex] = np.array(batch_coordinates)

	# Define body parts
	body_parts = ['head_top', 'upper_neck', 'right_shoulder', 'right_elbow', 'right_wrist', 'thorax', 'left_shoulder', 'left_elbow', 'left_wrist', 'pelvis', 'right_hip', 'right_knee', 'right_ankle', 'left_hip', 'left_knee', 'left_ankle']

	assert (coordinates < 0).sum() == 0
	assert (coordinates > 1).sum() == 0
	coordinates = np.int32(coordinates * [frame_height, frame_width])
	result = {
		"coordinates" : coordinates,
		"body_parts" : body_parts
	}

	vkeCache.set(key, result)
	dprint("[EfficientPose%s::vidGetBodyKeypoints] Stored keypoints to %s" % (model.modelType, key))
	return result