import os
import numpy as np
import simple_caching
from pathlib import Path

# The default dir is in the root of the project, under ".db" directory
vkeCacheDir = Path(__file__).absolute().parents[1] / ".db"
vkeCache = simple_caching.NpyFS(vkeCacheDir)

def videoGetDescriptor(video):
	# verySmart(TM)
	descriptorFirst = "%s-%s-%s" % (video[0].mean(), np.median(video[0]), video[0].std())
	descriptorSecond = "%s-%s-%s" % (video[1].mean(), np.median(video[1]), video[1].std())
	videoDescriptor = "%d-%s-%s" % (len(video), descriptorFirst, descriptorSecond)
	return videoDescriptor
