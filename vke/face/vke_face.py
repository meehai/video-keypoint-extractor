from abc import ABC, abstractmethod

class VKE_Face(ABC):
    @abstractmethod
    def imgGetFaceKeypoints(image):
        pass

    @abstractmethod
    def imgGetFaceBbox(image):
        pass
    
    @abstractmethod
    def vidGetFaceKeypoints(video):
        pass

    @abstractmethod
    def vidGetFaceBbox(video):
        pass
