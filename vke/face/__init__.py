from .face_alignment import VKE_FaceAlignment
from ..utils import dprint

Objs = {}

def getLib(faceLib):
    global Objs
    if faceLib in Objs:
        return Objs[faceLib]

    if faceLib == "face_alignment":
        dprint("[face::getLib] Instantiating face_alignment object.")
        Objs[faceLib] = VKE_FaceAlignment()
    
    return Objs[faceLib]

def imgGetFaceBbox(image, faceLib):
    return getLib(faceLib).imgGetFaceBbox(image)

def vidGetFaceBbox(video, faceLib):
    return getLib(faceLib).vidGetFaceBbox(video)

def imgGetFaceKeypoints(image, faceLib):
    return getLib(faceLib).imgGetFaceKeypoints(image)

def vidGetFaceKeypoints(video, faceLib):
    return getLib(faceLib).vidGetFaceKeypoints(video)
