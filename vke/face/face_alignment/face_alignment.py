import torch as tr
from .bbox import imgGetFaceBbox, vidGetFaceBbox
from .keypoints import imgGetFaceKeypoints, vidGetFaceKeypoints

class VKE_FaceAlignment:
    def __init__(self):
        import face_alignment
        device = "cuda" if tr.cuda.is_available() else "cpu"
        self.fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, flip_input=False, device=device)

    def imgGetFaceBbox(self, image):
        return imgGetFaceBbox(self.fa, image)
    
    def vidGetFaceBbox(self, video):
        return vidGetFaceBbox(self.fa, video)
    
    def imgGetFaceKeypoints(self, image):
        return imgGetFaceKeypoints(self.fa, image)
    
    def vidGetFaceKeypoints(self, video):
        return vidGetFaceKeypoints(self.fa, video)