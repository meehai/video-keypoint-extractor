import os
import face_alignment
import numpy as np
from pathlib import Path
from tqdm import trange
from ...utils import videoGetDescriptor, vkeCache, drange, dprint

def imgGetFaceKeypoints(fa, img):
	preds = fa.get_landmarks(img)
	if preds is None:
		return None
	preds = np.clip(preds[0], (0, 0), (img.shape[0] - 1, img.shape[1] - 1))
	return preds.astype(np.int32)

def vidGetFaceKeypoints(fa, video):
	key = "%s/face_alignment_face_kp.npy" % videoGetDescriptor(video)
	if vkeCache.check(key):
		vidKeypoints = vkeCache.get(key)
		assert len(vidKeypoints) == len(video)
		dprint("[face_alignment::vidGetFaceKeypoints] Loaded keypoints from %s" % key)
		return vidKeypoints

	dprint("[face_alignment::vidGetFaceKeypoints] No precomputed keypoints. Computing...")
	N = len(video)
	results = np.zeros((N, 68, 2), dtype=np.int32)
	nones = []
	for i in drange(N, desc="[face_alignment::vidGetFaceKeypoints] Keypoints"):
		img = video[i]
		keypoints = imgGetFaceKeypoints(fa, img)
		if keypoints is None:
			nones.append(i)
			if i > 0:
				results[i] = results[i - 1]
		else:
			results[i] = keypoints
	
	if len(nones) > N * 100 / 5:
		dprint("[face_alignment::vidGetFaceKeypoints] More than 5%% of video has no faces. Skipping.")
		return None
	
	dprint("[face_alignment::vidGetFaceKeypoints] Stored to %s" % key)
	vkeCache.set(key, results)
	return results
