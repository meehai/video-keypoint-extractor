import numpy as np
import os
from pathlib import Path
from ...utils import videoGetDescriptor, vkeCache, dprint, drange

def imgGetFaceBbox(fa, image):
	try:
		detected_faces = fa.face_detector.detect_from_image(image.copy())
	except Exception as e:
		print("[imgGetFaceBbox] %s" % e)
		return None
	
	if detected_faces is None or len(detected_faces) == 0:
		print("[imgGetFaceBbox] No face detected. Skipping.")
		return None

	d = detected_faces[0]
	H, W = image.shape[0], image.shape[1]
	d = np.int32(d)[0 : 4]
	d = np.clip(d, [0, 0, 0, 0], [W, H, W, H])
	x1, y1, x2, y2 = d
	res = np.array([x1, x2, y1, y2])
	return res

def vidGetFaceBbox(fa, video):
	key = "%s/face_alignment_face_bbox.npy" % videoGetDescriptor(video)
	if vkeCache.check(key):
		vidKeypoints = vkeCache.get(key)
		assert len(vidKeypoints) == len(video)
		dprint("[face_alignment::vidGetFaceBbox] Loaded bbox from %s" % key)
		return vidKeypoints

	dprint("[face_alignment::vidGetFaceBbox] No precomputed bbox. Computing...")
	N = len(video)
	results = np.zeros((N, 4), dtype=np.int32)
	nones = []
	for i in drange(N, desc="[face_alignment::vidGetFaceBbox] Bounding box"):
		img = video[i]
		keypoints = imgGetFaceBbox(fa, img)
		if keypoints is None:
			nones.append(i)
			if i > 0:
				results[i] = results[i - 1]
		else:
			results[i] = keypoints
	
	if len(nones) > N * 100 / 5:
		dprint("[face_alignment::vidGetFaceBbox] More than 5%% of video has no faces. Skipping.")
		return None
	
	dprint("[face_alignment::vidGetFaceBbox] Stored to %s" % key)
	vkeCache.set(key, results)
	return results
